package com.igeek;

import org.springframework.beans.factory.FactoryBean;

public class FactoryBean4 implements FactoryBean<Bean4>{

	@Override
	public Bean4 getObject() throws Exception {
		// TODO Auto-generated method stub
		//可以写一些初始化数据
		return new Bean4();
	}

	/***
	 * 返回类型
	 */
	@Override 
	public Class<?> getObjectType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSingleton() {
		// TODO Auto-generated method stub
		return false;
	}

}
