package com.igeek;



import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {


	@Test
	public void test1()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Bean1 bean1 = (Bean1) ac.getBean("bean1");
		System.out.println(bean1);
	}
	
	@Test
	public void test2()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Bean1 bean2 = (Bean1) ac.getBean("bean2");
		System.out.println(bean2);
	}
	
	@Test
	public void test3()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Bean3 bean3 = (Bean3) ac.getBean("bean3");
		System.out.println(bean3);
	}
	
	
	@Test
	public void test4()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Bean4 bean4 = (Bean4) ac.getBean("bean4");
		System.out.println(bean4);
	}
	
	
}
