package com.igeek.life;

public class LifeCycleBean {

	//成员变量
	private String name="tom"; //1. 赋值成员变量
	
	
	
	public LifeCycleBean() {
		super();
		// TODO Auto-generated constructor stub
		System.out.println("LifeCycleBean 构造器被调用了");
		//2. 通过构造方法初始化
		this.name="Jerry";
	}
	
	//构造方法调用后执行此方法，法国法名随意，需要在xml中配置后才能生效
	public void init()
	{
		System.out.println("LifeCycleBean init 调用");
	}
	
	public void save() {
		// TODO Auto-generated method stub
		System.out.println("执行了save 方法");
		//3.单独的初始化方法进行初始化数据
		this.name="jack";
	}
	
	public void destroy()
	{
		System.out.println("LifeCycleBean destory 调用");
		System.out.println(name);
	}
	
}
