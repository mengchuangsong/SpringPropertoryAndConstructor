package com.igeek.life;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {
	
	@Test
	public void test()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		LifeCycleBean lifeBean = (LifeCycleBean) ac.getBean("lifeCycleBean");
		lifeBean.save();
		//为什么没有执行销毁方法
		//原因： 使用debug模式，jvm 直接就关了，spring 的容器还没有来得及销毁对象
		//解决： 手动销毁spring容器,只能销毁单例的对象
		((AbstractApplicationContext) ac).close();
	}
	

}
