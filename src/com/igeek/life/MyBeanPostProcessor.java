package com.igeek.life;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor{

	
	
	
	public MyBeanPostProcessor() {
		super();
		// TODO Auto-generated constructor stub
		System.out.println("MyBeanPostProcessor 执行构造方法！");
	}

	/***
	 * 在bean 初始化之后执行
	 * @param bean 返回的bean
	 * @param beanName bean的名称
	 * 
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		// TODO Auto-generated method stub
		if(beanName.equals("lifeCycleBean")) {
			System.out.println(beanName + "在初始化之后开始增强！");
		}
		return bean;
	}

	/***
	 * 初始化之前调用
	 * @param bean 返回的bean
	 * 
	 * @param beanName bean的名称
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		// TODO Auto-generated method stub
		if(beanName.equals("lifeCycleBean"))
		{
			System.out.println(beanName + "在初始化之前开始增强！");
		}
		return bean;
	}

}
