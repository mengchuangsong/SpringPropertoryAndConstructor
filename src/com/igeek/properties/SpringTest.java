package com.igeek.properties;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {

	@Test
	public void test()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Car car = (Car) ac.getBean("car");
		System.out.println(car);
	}
	
	
	@Test
	public void test1()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Person person = (Person) ac.getBean("person2");
		System.out.println(person);
	}
	
	
	@Test
	public void test3()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Person person = (Person) ac.getBean("person3");
		System.out.println(person);
	}
	
	@Test
	public void test4()
	{
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		CollectionBean collectionBean = (CollectionBean) ac.getBean("collectionBean");
		System.out.println(collectionBean);
	}
	
	@Test
	public void test5()
	{
//		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml","applicationContext2.xml");
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		CollectionBean collectionBean = (CollectionBean) ac.getBean("collectionBean");
		System.out.println(collectionBean);
	}
	
}
