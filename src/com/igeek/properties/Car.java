package com.igeek.properties;


/***
 * ����������
 * @author Administrator
 *
 */
public class Car {

	private Integer id;
	private String name;
	private Double price;
	
	

	public void setId(Integer id) {
		this.id = id;
	}




	public void setName(String name) {
		this.name = name;
	}




	public void setPrice(Double price) {
		this.price = price;
	}




	public Integer getId() {
		return id;
	}




	public String getName() {
		return name;
	}




	public Double getPrice() {
		return price;
	}




	public Car(Integer id, String name, Double price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}




	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + ", price=" + price + "]";
	}
	
	
}
