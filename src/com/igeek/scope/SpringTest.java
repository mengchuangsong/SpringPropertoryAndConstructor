package com.igeek.scope;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {

	@Test
	public void testBean()
	{
		//1. 先构建实例化获取spring容器
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2. 看看多次获取bean的时候，是不是同一个
		SingleBean singleBean1 = (SingleBean) ac.getBean("singleBean");
		SingleBean singleBean2 = (SingleBean) ac.getBean("singleBean");
		System.out.println(singleBean1);
		System.out.println(singleBean2);
		PrototypeBean prototype1 = (PrototypeBean) ac.getBean("prototyBean");
		PrototypeBean prototype2 = (PrototypeBean) ac.getBean("prototyBean");
		System.out.println(prototype1);
		System.out.println(prototype2);
	}
}
